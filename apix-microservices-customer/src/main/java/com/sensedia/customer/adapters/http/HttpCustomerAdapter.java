package com.sensedia.customer.adapters.http;

import com.sensedia.customer.adapters.dtos.CustomerCreationDto;
import com.sensedia.customer.adapters.dtos.CustomerDto;
import com.sensedia.customer.adapters.mappers.CustomerMapper;
import com.sensedia.customer.domains.Customer;
import com.sensedia.customer.ports.ApplicationPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/${BASE_PATH}/customers")
public class HttpCustomerAdapter {

    private final ApplicationPort customerApplication;
    private final CustomerMapper customerMapper;

    @Autowired
    public HttpCustomerAdapter(
            ApplicationPort customerApplication, CustomerMapper customerMapper) {
        this.customerApplication = customerApplication;
        this.customerMapper = customerMapper;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody CustomerCreationDto customerCreationDto) {
        Customer customerMapper = this.customerMapper.toCustomer(customerCreationDto);
        Customer customer = customerApplication.create(customerMapper);

        return ResponseEntity.created(buildLocation(customer.getId())).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDto> get(@PathVariable String id) {
        Customer customer = customerApplication.findById(id);

        CustomerDto customerDto = customerMapper.toCustomerDto(customer);

        return ResponseEntity.ok(customerDto);
    }

    private URI buildLocation(String id) {
        return ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/${BASE_PATH}/customers/{id}")
                .buildAndExpand(id)
                .toUri();
    }
}
