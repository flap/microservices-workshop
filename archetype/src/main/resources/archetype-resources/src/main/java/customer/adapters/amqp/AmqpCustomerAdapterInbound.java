package ${package}.customer.adapters.amqp;

import ${package}.commons.errors.resolvers.ExceptionResolver;
import ${package}.customer.adapters.amqp.config.BindConfig;
import ${package}.customer.adapters.amqp.config.BrokerInput;
import ${package}.customer.adapters.dtos.CustomerCreationDto;
import ${package}.customer.adapters.mappers.CustomerMapper;
import ${package}.customer.domains.Customer;
import ${package}.customer.ports.AmqpPort;
import ${package}.customer.ports.ApplicationPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

import javax.validation.Valid;

@EnableBinding(BrokerInput.class)
@Slf4j
public class AmqpCustomerAdapterInbound {

    private final ApplicationPort applicationPort;
    private final CustomerMapper customerMapper;
    private final AmqpPort amqpPort;
    private final ExceptionResolver exceptionResolver;

    public AmqpCustomerAdapterInbound(
            ApplicationPort applicationPort,
            CustomerMapper customerMapper, AmqpPort amqpPort, ExceptionResolver exceptionResolver) {
        this.applicationPort = applicationPort;
        this.customerMapper = customerMapper;
        this.amqpPort = amqpPort;
        this.exceptionResolver = exceptionResolver;
    }

    @StreamListener(target = BindConfig.SUBSCRIBE_CUSTOMER_CREATION_REQUESTED)
    public void subscribeExchangeCustomerCreationRequested(CustomerCreationDto customerCreationDto) {
        try {
            log.info("Customer received: " + customerCreationDto.toString());
            Customer customer = this.applicationPort.create(this.customerMapper.toCustomer(customerCreationDto));
            log.info("Customer created: " + customer.getId());
        } catch (Exception e) {
            log.error("Error in amqp port inbound customer creation requested: " + e.getMessage());
            amqpPort.notifyCustomerOperationError(
                    exceptionResolver.solve(e).addOriginalMessage(customerCreationDto));
        }
    }
}
