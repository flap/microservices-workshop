package ${package}.customer.adapters.amqp.config;

public class BindConfig {

  public static final String SUBSCRIBE_CUSTOMER_CREATION_REQUESTED = "subscribeCustomerCreationRequested";
  public static final String PUBLISH_CUSTOMER_CREATED = "publishCustomerCreated";
  public static final String PUBLISH_CUSTOMER_OPERATION_ERROR = "publishCustomerOperationError";

  private BindConfig() {}
}
