package ${package}.customer.adapters.amqp.config;

public class EventConfig {

  public static final String CUSTOMER_CREATION_EVENT_NAME = "CustomerCreation";
  public static final String CUSTOMER_OPERATION_ERROR_EVENT_NAME = "CustomerOperationError";

  private EventConfig() {}
}
