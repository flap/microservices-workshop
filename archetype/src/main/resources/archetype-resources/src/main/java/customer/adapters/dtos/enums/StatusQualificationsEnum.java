package ${package}.customer.adapters.dtos.enums;

import lombok.Getter;

@Getter
public enum StatusQualificationsEnum {

    APPROVED("approved"),
    DENIED("denied");

    private final String status;

    StatusQualificationsEnum(String status) {
        this.status = status;
    }
}
