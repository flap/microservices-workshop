package ${package}.customer.adapters.http;

import ${package}.customer.adapters.dtos.CustomerCreationDto;
import ${package}.customer.adapters.dtos.CustomerDto;
import ${package}.customer.adapters.mappers.CustomerMapper;
import ${package}.customer.domains.Customer;
import ${package}.customer.ports.ApplicationPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customers")
public class HttpCustomerAdapter {

  private final ApplicationPort customerApplication;
  private final CustomerMapper customerMapper;

  @Autowired
  public HttpCustomerAdapter(
          ApplicationPort customerApplication, CustomerMapper customerMapper) {
    this.customerApplication = customerApplication;
    this.customerMapper = customerMapper;
  }

  @PostMapping
  public ResponseEntity<CustomerDto> create(@RequestBody CustomerCreationDto customerCreationDto) {
    Customer customerMapper = this.customerMapper.toCustomer(customerCreationDto);
    Customer customer = customerApplication.create(customerMapper);

    CustomerDto customerDto = this.customerMapper.toCustomerDto(customer);

    return ResponseEntity.status(HttpStatus.CREATED).body(customerDto);
  }

  @GetMapping("/{id}")
  public ResponseEntity<CustomerDto> get(@PathVariable String id) {
    Customer customer = customerApplication.findById(id);

    CustomerDto customerDto = customerMapper.toCustomerDto(customer);

    return ResponseEntity.ok(customerDto);
  }
}
