package ${package}.customer.adapters.mappers;

import ${package}.customer.adapters.dtos.CustomerCreationDto;
import ${package}.customer.adapters.dtos.CustomerDto;
import ${package}.customer.domains.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
  Customer toCustomer(CustomerCreationDto customerCreationDto);

  CustomerDto toCustomerDto(Customer customer);

}
