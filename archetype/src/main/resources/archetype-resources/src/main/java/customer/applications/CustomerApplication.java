package ${package}.customer.applications;

import ${package}.commons.errors.exceptions.BusinessException;
import ${package}.commons.errors.exceptions.NotFoundException;
import ${package}.customer.adapters.dtos.QualificationsDtoResponse;
import ${package}.customer.adapters.http.QualificationsClient;
import ${package}.customer.domains.Customer;
import ${package}.customer.ports.AmqpPort;
import ${package}.customer.ports.ApplicationPort;
import ${package}.customer.ports.RepositoryPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

import static ${package}.customer.adapters.dtos.enums.StatusQualificationsEnum.APPROVED;

@Service
@Slf4j
public class CustomerApplication implements ApplicationPort {

    private final AmqpPort amqpPort;
    private final RepositoryPort repository;
    private final QualificationsClient qualificationsClient;

    @Autowired
    public CustomerApplication(AmqpPort amqpPort, RepositoryPort repository,
                               QualificationsClient qualificationsClient) {
        this.amqpPort = amqpPort;
        this.repository = repository;
        this.qualificationsClient = qualificationsClient;
    }

    @Override
    public Customer create(@Valid @NotNull Customer customer) {
        log.info("Creating customer with name: " + customer.getFirstName());
        if (this.isQualificationCustomerApproved(customer)) {
            log.info("Customer approved: " + customer.getFirstName());

            customer.setId(UUID.randomUUID().toString());
            customer.setUpdatedAt(Instant.now());
            customer.setCreatedAt(Instant.now());
            repository.save(customer);
            amqpPort.notifyCustomerCreation(customer);

            return customer;
        }

        log.info("Customer denied: " + customer.getFirstName());
        throw new BusinessException("Customer can't create account.", "Business", "Customer");
    }

    public boolean isQualificationCustomerApproved(Customer customer) {
        ResponseEntity<QualificationsDtoResponse> response = this.qualificationsClient.qualify(customer);

        if (response.getStatusCode().is2xxSuccessful() && response.hasBody()) {
            return APPROVED.getStatus().equalsIgnoreCase(Objects.requireNonNull(response.getBody()).getStatus());
        }

        return false;
    }

    @Override
    public Customer findById(@NotNull String id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Customer not found"));
    }
}
