package ${package}.customer.ports;

import ${package}.commons.errors.domains.DefaultErrorResponse;
import ${package}.customer.domains.Customer;

public interface AmqpPort {

  void notifyCustomerCreation(Customer customer);

  void notifyCustomerOperationError(DefaultErrorResponse errorResponse);
}
