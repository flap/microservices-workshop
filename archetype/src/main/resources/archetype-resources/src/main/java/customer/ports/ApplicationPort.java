package ${package}.customer.ports;

import ${package}.customer.domains.Customer;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface ApplicationPort {
  Customer create(@Valid @NotNull Customer customer);

  Customer findById(@NotNull String id);

}
